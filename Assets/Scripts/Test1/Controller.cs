using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Test1
{
    public class Controller : MonoBehaviour
    {
        public Model _model;
        public Button _button;

        private void Awake()
        {
            _button.onClick.AddListener(OnButtonClicked);
        }

        public void OnButtonClicked()
        {
            _model.IncreaseCounter();
        }
    } 
}
