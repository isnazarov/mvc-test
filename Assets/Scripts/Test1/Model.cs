using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Test1
{
    public class Model : MonoBehaviour
    {
        public Action<int> OnClickCouterChange;

        private int _clickCount = 0;


        void Start()
        {
            OnClickCouterChange?.Invoke(_clickCount);
        }

        public void IncreaseCounter()
        {
            _clickCount++;
            OnClickCouterChange?.Invoke(_clickCount);
        }
    } 
}
