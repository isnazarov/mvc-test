using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Test1
{
    public class View : MonoBehaviour
    {
        public Model _model;
        public Text _counterTextField;

        private void Awake()
        {
            _model.OnClickCouterChange += OnUpdateClickCounter;
        }

        private void OnUpdateClickCounter(int clickCount)
        {
            _counterTextField.text = clickCount.ToString();
        }
    } 
}
