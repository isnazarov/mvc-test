using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test2
{
    public class Cube : MonoBehaviour
    {
        private float _velocity;
        private float _maxDistance;
        private float _currentDistance = 0f;

        void Update()
        {
            Move(Time.deltaTime);
        }

        public void Initialize(float velocity, float maxDistance)
        {
            _velocity = velocity;
            _maxDistance = maxDistance;
        }

        private void Move(float deltaTime)
        {
            Vector3 step = Vector3.forward * _velocity * deltaTime;
            transform.Translate(step);

            _currentDistance += step.magnitude;

            if (_currentDistance > _maxDistance)
                Destroy(gameObject);
        }
    } 
}
