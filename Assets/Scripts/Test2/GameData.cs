using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Test2
{
    public class GameData : MonoBehaviour
    {
        public Action<float> OnSpawnPeriodChange;
        public Action<float> OnCubeVelocityChange;
        public Action<float> OnCubeMaxDistanceChange;

        public float SpawnPeriod { get; private set; }
        public float CubeVelocity { get; private set; }
        public float CubeMaxDistance { get; private set; }

        public void SetSpawnPeriod(float spawnPeriod)
        {
            SpawnPeriod = spawnPeriod;
            OnSpawnPeriodChange?.Invoke(spawnPeriod);
        }

        public void SetCubeVelocity(float velocity)
        {
            CubeVelocity = velocity;
            OnCubeVelocityChange?.Invoke(velocity);
        }

        public void SetCubeMaxDistance(float distance)
        {
            CubeMaxDistance = distance;
            OnCubeMaxDistanceChange?.Invoke(distance);
        }


    } 
}
