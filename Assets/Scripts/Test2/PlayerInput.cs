using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Globalization;

namespace Test2
{
    public class PlayerInput : MonoBehaviour
    {
        public GameData _gameData;

        public InputField _spawnPeriodInputField;
        public InputField _velocityInputField;
        public InputField _maxDistanceInputField;

        private CultureInfo _cultureInfo;

        void Awake()
        {
            _cultureInfo = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            _cultureInfo.NumberFormat.CurrencyDecimalSeparator = ".";

            _spawnPeriodInputField.onSubmit.AddListener(OnSpawnPeriodChange);
            _velocityInputField.onSubmit.AddListener(OnVelocityChange);
            _maxDistanceInputField.onSubmit.AddListener(OnMaxDistanceChange);
        }

        private void Start()
        {
            OnSpawnPeriodChange(_spawnPeriodInputField.text);
            OnVelocityChange(_velocityInputField.text);
            OnMaxDistanceChange(_maxDistanceInputField.text);
        }

        private float StringToFloat(string str)
        {
            return float.Parse(str, NumberStyles.Any, _cultureInfo);
        }

        private void OnSpawnPeriodChange(string str)
        {
            _gameData.SetSpawnPeriod(StringToFloat(str));
        }

        private void OnVelocityChange(string str)
        {
            _gameData.SetCubeVelocity(StringToFloat(str));
        }

        private void OnMaxDistanceChange(string str)
        {
            _gameData.SetCubeMaxDistance(StringToFloat(str));
        }

    }

}