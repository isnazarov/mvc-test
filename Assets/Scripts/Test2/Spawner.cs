using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test2
{
    public class Spawner : MonoBehaviour
    {
        public GameData _gameData;

        private float _spawnTimer = 0f;

        private void Update()
        {
            _spawnTimer += Time.deltaTime;

            if(_spawnTimer > _gameData.SpawnPeriod)
            {
                _spawnTimer = 0f;
                Spawn();
            }
        }

        public void Spawn()
        {
            Cube cube = GameObject.CreatePrimitive(PrimitiveType.Cube).AddComponent<Cube>();
            cube.transform.SetPositionAndRotation(transform.position, transform.rotation);
            cube.Initialize(_gameData.CubeVelocity, _gameData.CubeMaxDistance);
        }
    } 
}
