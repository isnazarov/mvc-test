

namespace Test3
{
    interface ISomeInterface
    {
        void Call();
    }

    // Option 1.
    // We can change struct to class, but it can contradict our architecture.
    struct SomeStruct : ISomeInterface
    {
        public void Call()
        { }
    }

    class SomeClass
    {
        public void Run()
        {
            var someStruct = new SomeStruct();

            // Option 2.
            // We can explicitly cast to ISomeInterface and pass it to SomeMethod,
            // but we have to unbox it:  
            //      ISomeInterface someStructAsInterface = someStruct;
            //      SomeMethod(someStructAsInterface);
            //      someStruct = (SomeStruct)someStructAsInterface;


            // Option 3.
            // Create an alternative method SomeMethod and pass the variable there by its reference.
            // Although it leads to boilerplate code, I think it is the best option, because boxing is not used here. 
            SomeMethod(ref someStruct);
        }
        public void SomeMethod(ISomeInterface @interface)
        {
            @interface.Call();
        }
        
        public void SomeMethod(ref SomeStruct s)
        {
            s.Call();
        }

    }

}